package misclases;

public class Terreno {
    private float ancho;
    private float largo;
    
    //Metodos constructores
    //Por omisión
    Terreno(){
    this.ancho = 0;
    this.largo = 0;
    }
    //Por parametros
    Terreno(float ancho, float largo){
        this.ancho = ancho;
        this.largo = largo;
    }
    //Por copia
    Terreno(Terreno x){
        this.ancho = x.ancho;
        this.largo = x.largo;       
    }
    //Metodos set y get

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }
    
    public float calcularPerimetro(){
     float perimetro = 0.0f; 
     perimetro = this.ancho * 2 + this.largo * 2;
     return perimetro;
    }
    
    public float calcularArea(){
        float area = 0.0f;
        area = this.ancho * this.largo;
        return area;
    }
    
    public void imprimirTerreno(){
        System.out.println("Ancho = " + this.ancho);
        System.out.println("Largo = " + this.largo);
        System.out.println("El perimetro es " + this.calcularPerimetro());
        System.out.println("El area es " + this.calcularArea());
    }   
}
    