/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

import javax.swing.JOptionPane;

/**
 *
 * @author angel
 */
public class jintCotizacion extends javax.swing.JInternalFrame {

    /**
     * Creates new form jintCotizacion
     */
    public jintCotizacion() {
        initComponents();
        this.resize(843, 555);
        this.deshabilitar();
    }

    public void deshabilitar(){
        this.txtDescripcion.setEnabled(false);
        this.txtNum.setEnabled(false);
        this.txtPrecio.setEnabled(false);
        this.txtPorcentaje.setEnabled(false);
        this.txtPagoMensual.setEnabled(false);
        this.txtPago.setEnabled(false);
        this.txtTotal.setEnabled(false);
    
        //botones
        this.btnGuardar.setEnabled(false);
        this.btnMostrar.setEnabled(false);
    }
    
    public void habilitar(){
        this.txtDescripcion.setEnabled(!false);
        this.txtNum.setEnabled(!false);
        this.txtPrecio.setEnabled(!false);
        this.txtPorcentaje.setEnabled(!false);
        
    //botones
        this.btnGuardar.setEnabled(!false);
        this.btnMostrar.setEnabled(!false);
    }
    
    public void limpiar(){
        this.txtDescripcion.setText("");
        this.txtNum.setText("");
        this.txtPago.setText("");
        this.txtPagoMensual.setText("");
        this.txtPorcentaje.setText("");
        this.txtPrecio.setText("");
        this.txtTotal.setText("");
        
        //poner en la primera de posicion al combobox
        this.cmbPlazos.setSelectedIndex(0);
        
        //Poner el cursor en el numCotizacion
        this.txtNum.requestFocus();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNum = new javax.swing.JTextField();
        txtDescripcion = new javax.swing.JTextField();
        txtPrecio = new javax.swing.JTextField();
        txtPorcentaje = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        cmbPlazos = new javax.swing.JComboBox<>();
        btnGuardar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtPagoMensual = new javax.swing.JTextField();
        txtPago = new javax.swing.JTextField();
        txtTotal = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(51, 255, 255));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Cotizacion");
        setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        setMinimumSize(new java.awt.Dimension(843, 555));
        getContentPane().setLayout(null);

        jLabel2.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(204, 255, 255));
        jLabel2.setText("Porcentaje de pago inicial :");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(30, 170, 240, 30);

        jLabel3.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(204, 255, 255));
        jLabel3.setText("Descripción del automovil :");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(30, 70, 240, 30);

        jLabel4.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(204, 255, 255));
        jLabel4.setText("Plazos :");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(200, 230, 70, 20);

        txtNum.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txtNum.setForeground(new java.awt.Color(51, 51, 51));
        txtNum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumActionPerformed(evt);
            }
        });
        getContentPane().add(txtNum);
        txtNum.setBounds(280, 20, 380, 28);

        txtDescripcion.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txtDescripcion.setForeground(new java.awt.Color(51, 51, 51));
        txtDescripcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDescripcionActionPerformed(evt);
            }
        });
        getContentPane().add(txtDescripcion);
        txtDescripcion.setBounds(280, 70, 380, 28);

        txtPrecio.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txtPrecio.setForeground(new java.awt.Color(51, 51, 51));
        txtPrecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrecioActionPerformed(evt);
            }
        });
        getContentPane().add(txtPrecio);
        txtPrecio.setBounds(280, 120, 380, 30);

        txtPorcentaje.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txtPorcentaje.setForeground(new java.awt.Color(51, 51, 51));
        txtPorcentaje.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPorcentajeActionPerformed(evt);
            }
        });
        getContentPane().add(txtPorcentaje);
        txtPorcentaje.setBounds(280, 170, 380, 30);

        jLabel5.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(204, 255, 255));
        jLabel5.setText("Precio :");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(200, 120, 80, 30);

        cmbPlazos.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        cmbPlazos.setForeground(new java.awt.Color(51, 51, 51));
        cmbPlazos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "12 meses", "24 meses", "36 meses", "48 meses", "60 meses" }));
        cmbPlazos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbPlazosActionPerformed(evt);
            }
        });
        getContentPane().add(cmbPlazos);
        cmbPlazos.setBounds(280, 220, 110, 40);

        btnGuardar.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(690, 90, 100, 40);

        btnCerrar.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(660, 440, 110, 40);

        btnNuevo.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(690, 20, 100, 40);

        btnLimpiar.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(90, 440, 110, 40);

        btnMostrar.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(690, 160, 100, 40);

        btnCancelar.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(370, 440, 110, 40);

        jPanel1.setBackground(new java.awt.Color(0, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculos de la cotizacion", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 16))); // NOI18N
        jPanel1.setLayout(null);

        jLabel9.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(51, 51, 51));
        jLabel9.setText("Pago Mensual :");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(60, 100, 110, 30);

        jLabel11.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(51, 51, 51));
        jLabel11.setText("Pago Inicial :");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(80, 20, 90, 30);

        jLabel12.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(51, 51, 51));
        jLabel12.setText("Total a Financiar :");
        jPanel1.add(jLabel12);
        jLabel12.setBounds(40, 60, 130, 30);

        txtPagoMensual.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txtPagoMensual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoMensualActionPerformed(evt);
            }
        });
        jPanel1.add(txtPagoMensual);
        txtPagoMensual.setBounds(200, 100, 360, 30);

        txtPago.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txtPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoActionPerformed(evt);
            }
        });
        jPanel1.add(txtPago);
        txtPago.setBounds(200, 20, 360, 30);

        txtTotal.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        jPanel1.add(txtTotal);
        txtTotal.setBounds(200, 60, 360, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(90, 270, 680, 150);

        jLabel10.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(204, 255, 255));
        jLabel10.setText("Numero de cotización :");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(70, 20, 210, 30);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 255, 255));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo.jpg"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(-140, -10, 980, 540);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumActionPerformed

    private void txtDescripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescripcionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescripcionActionPerformed

    private void txtPrecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrecioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrecioActionPerformed

    private void txtPagoMensualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoMensualActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoMensualActionPerformed

    private void cmbPlazosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbPlazosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbPlazosActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        //hacer una validacion
        boolean exito = false;
        if(this.txtNum.getText().equals("")) exito = true;
        if(this.txtDescripcion.getText().equals("")) exito = true;
        if(this.txtPorcentaje.getText().equals("")) exito = true;
        if(this.txtPrecio.getText().equals("")) exito = true;
        
        if(exito == true){
            //falto informacion
            JOptionPane.showMessageDialog(this, "Falto capturar información");
        }
        else {
            //todo bien
            con.setNumCotizacion(Integer.parseInt(this.txtNum.getText()));
            con.setDescripcionAutomovil(this.txtDescripcion.getText());
            switch(this.cmbPlazos.getSelectedIndex()){
            case 0:
               con.setPlazo(12);
               break;
            case 1:
               con.setPlazo(24);
               break;
            case 2:
               con.setPlazo(36);
               break;
            case 3:
               con.setPlazo(48);
               break;
            case 4:
               con.setPlazo(60);
               break;
        }
            con.setPorcentajePagoInicial(Float.parseFloat(this.txtPorcentaje.getText()));
            con.setPrecio(Float.parseFloat(this.txtPrecio.getText()));
            
            JOptionPane.showMessageDialog(this, "Se guardó la informacion con exito");
            this.btnMostrar.setEnabled(true);
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        int opcion = 0;
        
        opcion = JOptionPane.showConfirmDialog(this, "¿Realmente quieres cerrar?", "Cotizacion", JOptionPane.YES_NO_OPTION);
        
        if (opcion == JOptionPane.YES_OPTION){
        
            this.dispose();
    }
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
        
        //mostrar la informacion del objeto
        
        this.txtNum.setText(String.valueOf(con.getNumCotizacion()));
        this.txtDescripcion.setText(con.getDescripcionAutomovil());
        this.txtPorcentaje.setText(String.valueOf(con.getPorcentajePagoInicial()));
        this.txtPrecio.setText(String.valueOf(con.getPrecio()));
        switch(con.getPlazo()){
            case 12: this.cmbPlazos.setSelectedIndex(0); break;
            case 24: this.cmbPlazos.setSelectedIndex(1); break;
            case 36: this.cmbPlazos.setSelectedIndex(2); break;
            case 48: this.cmbPlazos.setSelectedIndex(3); break;
            case 60: this.cmbPlazos.setSelectedIndex(4); break;
        }
        this.txtPago.setText(String.valueOf(con.calcularPagoInicial()));
        this.txtPagoMensual.setText(String.valueOf(con.calcularPagoMensual()));
        this.txtTotal.setText(String.valueOf(con.calcularTotalAFinanciar()));
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.deshabilitar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtPorcentajeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPorcentajeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPorcentajeActionPerformed

    private void txtPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoActionPerformed

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        con = new Cotizacion();
        this.habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cmbPlazos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtDescripcion;
    private javax.swing.JTextField txtNum;
    private javax.swing.JTextField txtPago;
    private javax.swing.JTextField txtPagoMensual;
    private javax.swing.JTextField txtPorcentaje;
    private javax.swing.JTextField txtPrecio;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
    private Cotizacion con;

    private void btnMostrar(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void btnGuardar(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
