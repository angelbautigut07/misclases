
package misclases;

public class Factura {
     private int numFactura;
     private String rfc;
     private String nombreCliente;
     private String domicilioFiscal;
     private String descripcion;
     private String fechaVenta;
     private float totalVenta;
     
   Factura(){
        this.numFactura = 0;
        this.rfc = "No especificado";
        this.nombreCliente = "No especificado";
        this.domicilioFiscal = "No especificado";
        this.descripcion = "No especificado";
        this.fechaVenta = "No especificado";
        this.totalVenta = 0;
   }

   Factura(int numFactura, String rfc, String nombreCliente, String domicilioFiscal, String descripcion, String fechaVenta, float totalVenta) {
        this.numFactura = numFactura;
        this.rfc = rfc;
        this.nombreCliente = nombreCliente;
        this.domicilioFiscal = domicilioFiscal;
        this.descripcion = descripcion;
        this.fechaVenta = fechaVenta;
        this.totalVenta = totalVenta;
    }
     
   Factura(Factura x){
       this.numFactura = x.numFactura;
        this.rfc = x.rfc;
        this.nombreCliente = x.nombreCliente;
        this.domicilioFiscal = x.domicilioFiscal;
        this.descripcion = x.descripcion;
        this.fechaVenta = x.fechaVenta;
        this.totalVenta = x.totalVenta;
   }

    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDomicilioFiscal() {
        return domicilioFiscal;
    }

    public void setDomicilioFiscal(String domicilioFiscal) {
        this.domicilioFiscal = domicilioFiscal;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(String fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public float getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(float totalVenta) {
        this.totalVenta = totalVenta;
    }
     
   public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.totalVenta * 0.16f;
     return impuesto;
   }
   
   public float calcularTotalAPagar(){
       float totalAPagar = 0.0f;
       totalAPagar = this.totalVenta + this.calcularImpuesto();
       return totalAPagar;
   }
   
   public void imprimirFactura(){
       System.out.println("Numero de factura: " + this.numFactura);
       System.out.println("RFC: " + this.rfc);
       System.out.println("Nombre de cliente: " + this.nombreCliente);
       System.out.println("Domicilio fiscal: " + this.domicilioFiscal);
       System.out.println("Descripcion: " + this.descripcion);
       System.out.println("Fecha de venta: " + this.fechaVenta);
       System.out.println("Total de venta: " + this.totalVenta);
       System.out.println("Impuesto: " + this.calcularImpuesto());
       System.out.println("Total a pagar: " + this.calcularTotalAPagar());
   }
}
