package misclases;

public class Cotizacion {
    private int numCotizacion;
    private int plazo;
    private String descripcionAutomovil;
    private float precio;
    private float porcentajePagoInicial;

    Cotizacion(){
        this.numCotizacion = 0;
        this.plazo = 0;
        this.descripcionAutomovil = "No especificado";
        this.precio = 0;
        this.porcentajePagoInicial = 0; 
    }
    
    Cotizacion(int numCotizacion, int plazo, String descripcionAutomovil, float precio, float porcentajePagoInicial){
        this.numCotizacion = numCotizacion;
        this.plazo = plazo;
        this.descripcionAutomovil = descripcionAutomovil;
        this.precio = precio;
        this.porcentajePagoInicial = porcentajePagoInicial; 
    }
    
    Cotizacion(Cotizacion x){
        this.numCotizacion = x.numCotizacion;
        this.plazo = x.plazo;
        this.descripcionAutomovil = x.descripcionAutomovil;
        this.precio = x.precio;
        this.porcentajePagoInicial = x.porcentajePagoInicial; 
    }

    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public String getDescripcionAutomovil() {
        return descripcionAutomovil;
    }

    public void setDescripcionAutomovil(String descripcionAutomovil) {
        this.descripcionAutomovil = descripcionAutomovil;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentajePagoInicial() {
        return porcentajePagoInicial;
    }

    public void setPorcentajePagoInicial(float porcentajePagoInicial) {
        this.porcentajePagoInicial = porcentajePagoInicial;
    }
    
    public float calcularPagoInicial(){
        float pagoInicial = 0.0f;
        pagoInicial = this.precio * this.porcentajePagoInicial / 100;
        return pagoInicial;
    }
    
    public float calcularTotalAFinanciar(){
        float totalAFinanciar = 0.0f;
        totalAFinanciar = this.precio - this.calcularPagoInicial();
        return totalAFinanciar;
    }
    
    public float calcularPagoMensual(){
        float pagoMensual = 0.0f;
        pagoMensual = this.calcularTotalAFinanciar() / this.plazo;
        return pagoMensual;
    }
    
    public void imprimirCotizacion(){
        System.out.println("Numero de cotizacion: " + this.numCotizacion);
        System.out.println("Plazo : " + this.plazo + " meses");
        System.out.println("Descripcion: " + this.descripcionAutomovil);
        System.out.println("Precio: " + this.precio);
        System.out.println("Porcentaje de pago inicial: " + this.porcentajePagoInicial + "%");
        System.out.println("Pago inicial: " + this.calcularPagoInicial());
        System.out.println("Total a financiar: " + this.calcularTotalAFinanciar());
        System.out.println("Pago mensual: " + this.calcularPagoMensual());
    }
}
