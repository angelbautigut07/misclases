/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author angel
 */
public class NotaVenta {
    private int numero;
    private String nombreCliente = null;
    private String concepto = null;
    private Fecha fechaventa;
    private float total;
    private int tipo; //1 Credito 2 Contado
    
    public NotaVenta(){
        this.numero = 0;
        this.nombreCliente = "";
        this.concepto = "";
        this.fechaventa = new Fecha();
        this.total = 0.0f;
        this.tipo = 1;                              
    }

    public NotaVenta(int numero, Fecha fechaventa, float total, int tipo) {
        this.numero = numero;
        this.fechaventa = fechaventa;
        this.total = total;
        this.tipo = tipo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Fecha getFechaventa() {
        return fechaventa;
    }

    public void setFechaventa(Fecha fechaventa) {
        this.fechaventa = fechaventa;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.total * 0.16f; 
        return impuesto;
    }
    
    public float calcularTotalAPagar(){
        float totalAPagar = 0.0f;
        totalAPagar = this.total + this.calcularImpuesto();
        return totalAPagar;
    }
}
