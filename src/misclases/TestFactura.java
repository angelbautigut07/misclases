
package misclases;

import java.util.Scanner;

public class TestFactura {

    public static void main(String[] args) {
        Factura factura = new Factura();
        Scanner sc = new Scanner(System.in);
        int opcion = 0, numFactura = 0;
        String rfc = "No especificado", nombreCliente = "No especificado", domicilioFiscal = "No especificado", descripcion = "No especificado", fechaVenta = "No especificado";
        float totalVenta = 0.0f;
        
        do{
            System.out.println("1.-Iniciar el objeto");
            System.out.println("2.-Cambiar numero de factura");
            System.out.println("3.-Cambiar rfc");
            System.out.println("4.-Cambiar nombre de cliente");
            System.out.println("5.-Cambiar domicilio fiscal");
            System.out.println("6.-Cambiar descripcion");
            System.out.println("7.-Cambiar fecha de venta");
            System.out.println("8.-Cambiar total de venta");
            System.out.println("9.-Mostrar informacion");
            System.out.println("10.-Salir");
            opcion = sc.nextInt();
            switch(opcion){
                case 1:
                    System.out.println("Escribe el numero de factura: ");
                    numFactura = sc.nextInt();
                    factura.setNumFactura(numFactura);
                    System.out.println("Escribe el rfc: ");
                    sc.nextLine();
                    rfc = sc.nextLine();  
                    factura.setRfc(rfc);
                    System.out.println("Escribe el nombre del cliente: ");
                    nombreCliente = sc.nextLine();
                    factura.setNombreCliente(nombreCliente);
                    System.out.println("Escribe el domicilio fiscal:");
                    domicilioFiscal = sc.nextLine();
                    factura.setDomicilioFiscal(domicilioFiscal);
                    System.out.println("Escribe la descripcion: ");
                    descripcion = sc.nextLine();
                    factura.setDescripcion(descripcion);
                    System.out.println("Escribe la fecha de venta: ");
                    fechaVenta = sc.nextLine();
                    factura.setFechaVenta(fechaVenta);
                    System.out.println("Escribe el total de venta: ");
                    totalVenta = sc.nextFloat();
                    factura.setTotalVenta(totalVenta);
                    break;
                case 2:
                    System.out.println("Escribe el numero de factura: ");
                    numFactura = sc.nextInt();
                    factura.setNumFactura(numFactura);
                    break;
                case 3:
                    System.out.println("Escribe el rfc: ");
                    rfc = sc.nextLine();  
                    factura.setRfc(rfc);
                    break;
                case 4: 
                    System.out.println("Escribe el nombre del cliente: ");
                    nombreCliente = sc.nextLine();
                    factura.setNombreCliente(nombreCliente);
                    break;
                case 5: 
                    System.out.println("Escribe el domicilio fiscal:");
                    domicilioFiscal = sc.nextLine();
                    factura.setDomicilioFiscal(domicilioFiscal);
                    break;
                case 6:
                    System.out.println("Escribe la descripcion: ");
                    descripcion = sc.nextLine();
                    factura.setDescripcion(descripcion);
                    break;
                case 7:
                     System.out.println("Escribe la fecha de venta: ");
                    fechaVenta = sc.nextLine();
                    factura.setFechaVenta(fechaVenta);
                    break;
                case 8:
                    System.out.println("Escribe el total de venta: ");
                    totalVenta = sc.nextFloat();
                    factura.setTotalVenta(totalVenta);
                    break;
                case 9:
                    factura.imprimirFactura();
                    break;
                case 10: 
                    System.out.println("Hasta luego");
                    break;
                default: 
                    System.out.println("Opcion invalida escriba una opcion del rango");
            }
        }while(opcion!=10);
                
        
    }
    
}
