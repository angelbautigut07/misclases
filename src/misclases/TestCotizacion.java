package misclases;

import java.util.Scanner;

public class TestCotizacion {
    public static void main(String[] args) {
     Cotizacion cotizacion = new Cotizacion();
     Scanner sc = new Scanner(System.in);
     int opcion = 0;
     float precio = 0.0f, porcentajePagoInicial = 0.0f, pagoInicial = 0.0f, totalAFinanciar = 0.0f, pagoMensual = 0;
     int numCotizacion = 0, plazo = 0;
     String descripcionAutomovil = "No especificado";
    
     do{
         System.out.println("1.-Iniciar el objeto");
         System.out.println("2.-Cambiar precio");
         System.out.println("3.-Cambiar porcentaje de pago inicial");
         System.out.println("4.-Cambiar numero de cotizacion");
         System.out.println("5.-Cambiar plazo");
         System.out.println("6.-Cambiar descripcion del automovil");
         System.out.println("7.-Mostrar informacion");
         System.out.println("8.-Salir");
         System.out.print("Escribe la opcion: ");
         opcion = sc.nextInt();
         
         switch(opcion){
             case 1:
                 System.out.print("Escribe el numero de cotizacion: ");
                 numCotizacion = sc.nextInt();
                 System.out.print("Escribe el plazo: ");
                 plazo = sc.nextInt();
                 System.out.print("Escribe la descripcion del automovil: ");
                 descripcionAutomovil = sc.next();
                 System.out.print("Escribe el precio: ");
                 precio = sc.nextFloat();
                 System.out.print("Escribe el porcentaje de pago inicial: ");
                 porcentajePagoInicial = sc.nextFloat();
                 cotizacion.setNumCotizacion(numCotizacion);
                 cotizacion.setPlazo(plazo);
                 cotizacion.setDescripcionAutomovil(descripcionAutomovil);
                 cotizacion.setPrecio(precio);
                 cotizacion.setPorcentajePagoInicial(porcentajePagoInicial);
                 break;
             case 2:
                 System.out.println("Escribe el precio: ");
                 precio = sc.nextFloat();
                 cotizacion.setPrecio(precio);
                 break;
             case 3: 
                 System.out.println("Escribe el porcentaje de pago inicial: ");
                 porcentajePagoInicial = sc.nextFloat();
                 cotizacion.setPorcentajePagoInicial(porcentajePagoInicial);
                 break;
             case 4:
                 System.out.println("Escribe el numero de cotizacion: ");
                 numCotizacion = sc.nextInt();
                 cotizacion.setNumCotizacion(numCotizacion);
                 break;
             case 5:
                 System.out.println("Escribe el plazo");
                 plazo = sc.nextInt();
                 cotizacion.setPlazo(plazo);
                 break;
             case 6:
                 System.out.println("Escribe la descripcion del automovil");
                 descripcionAutomovil = sc.nextLine();
                 cotizacion.setDescripcionAutomovil(descripcionAutomovil);
                 break;
             case 7:
                 cotizacion.imprimirCotizacion();
                 break;
             case 8:
                 System.out.println("Adios");
                 break;
             default:
                 System.out.println("Opcion invalida use una del rango valido");
         }
     }while(opcion!=8);
    }
}
